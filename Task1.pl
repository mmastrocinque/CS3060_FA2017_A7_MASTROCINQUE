%% 1. The person in fourth place has Avila as a surname.
%% 2. The person whose last name is Gillespie doesn't have the Norwegian penpal.
%% 3. The 5 people were  the person whose last name is Mercado, the person with the Palauan penpal, the person in eighteenth place, Asher,  and the person whose last name is Avila.
%% 4. The person whose last name is Rosa doesn't have the Argentinean penpal.
%% 5. The person whose last name is Mercado finished after the person with the Argentinean penpal.
%% 6. Mckenna finished before the person whose last name is Mercado.
%% 7. Either the person whose last name is Avila or the person whose last name is Gillespie has the Norwegian penpal.
%% 8. The person in ninth place does not have Vaughan as a last name.
%% 9. The person with the Jordanian penpal is Casey.
%% 10. Ashlyn finished before the person with the Palauan penpal.
%% 11. Of Tony and Mckenna, one came in tenth place and the other has Vaughan as a surnae

people(Ps) :-
	% each person in the list Ps of people is represented as:
	%      h(FirstName,PenPal,LastName)
	length(Ps, 5),

	%% Define attributes

	member(h(asher,_,_),Ps),
	member(h(ashlyn,_,_),Ps),
	member(h(casey,_,_),Ps),
	member(h(mckenna,_,_),Ps),
	member(h(tony,_,_),Ps),
	member(h(_,argentinean,_),Ps),
	member(h(_,jordanian,_),Ps),
	member(h(_,norweigian,_),Ps),
	member(h(_,palauan,_),Ps),
	member(h(_,zimbabwean,_),Ps),
	member(h(_,_,avila),Ps),
	member(h(_,_,gillespie),Ps),
	member(h(_,_,mercado),Ps),
	member(h(_,_,rosa),Ps),
	member(h(_,_,vaughan),Ps),
	
	Ps = [h(_,_,avila),_,_,_,_],                    	%%1

	\+member(h(_,norweigian,gillespie),Ps),				%%2

	\+member(h(asher,_,mercado),Ps),					%%3
	\+member(h(asher,palauan,_),Ps),
	\+member(h(asher,_,avila),Ps),
	\+member(h(_,palauan,mercado),Ps),
	\+member(h(_,palauan,avila),Ps),
	\+Ps=[_,_,_,_,h(asher,_,_)],
	\+Ps=[_,_,_,_,h(_,_,mercado)],
	\+Ps=[_,_,_,_,h(_,palauan,_)],
	\+Ps=[_,_,_,_,h(_,_,avila)],

	\+member(h(_,argentinean,rosa),Ps),					%%4

	(Ps=[_,_,_,h(_,argentinean,_),h(_,_,mercado)];		%%5
	Ps=[_,_,h(_,argentinean,_),_,h(_,_,mercado)];
	Ps=[_,_,h(_,argentinean,_),h(_,_,mercado),_];
	Ps=[_,h(_,argentinean,_),_,_,h(_,_,mercado)];
	Ps=[_,h(_,argentinean,_),_,h(_,_,mercado),_];
	Ps=[_,h(_,argentinean,_),h(_,_,mercado),_,_];
	Ps=[h(_,argentinean,_),_,_,_,h(_,_,mercado)];
	Ps=[h(_,argentinean,_),_,_,h(_,_,mercado),_];
	Ps=[h(_,argentinean,_),_,h(_,_,mercado),_,_];
	Ps=[h(_,argentinean,_),h(_,_,mercado),_,_,_]),

	(Ps=[_,_,_,h(mckenna,_,_),h(_,_,mercado)];		%%6
	Ps=[_,_,h(mckenna,_,_),_,h(_,_,mercado)];
	Ps=[_,_,h(mckenna,_,_),h(_,_,mercado),_];
	Ps=[_,h(mckenna,_,_),_,_,h(_,_,mercado)];
	Ps=[_,h(mckenna,_,_),_,h(_,_,mercado),_];
	Ps=[_,h(mckenna,_,_),h(_,_,mercado),_,_];
	Ps=[h(mckenna,_,_),_,_,_,h(_,_,mercado)];
	Ps=[h(mckenna,_,_),_,_,h(_,_,mercado),_];
	Ps=[h(mckenna,_,_),_,h(_,_,mercado),_,_];
	Ps=[h(mckenna,_,_),h(_,_,mercado),_,_,_]),

	member(h(_,norweigian,avila),Ps),		    	%%7

	\+Ps=[_,_,h(_,_,vaughan),_,_],					%%8

	member(h(casey,jordanian,_),Ps),				%%9

	(Ps=[_,_,_,h(ashlyn,_,_),h(_,palauan,_)];		%%10
	Ps=[_,_,h(ashlyn,_,_),_,h(_,palauan,_)];
	Ps=[_,_,h(ashlyn,_,_),h(_,palauan,_),_];
	Ps=[_,h(ashlyn,_,_),_,_,h(_,palauan,_)];
	Ps=[_,h(ashlyn,_,_),_,h(_,palauan,_),_];
	Ps=[_,h(ashlyn,_,_),h(_,palauan,_),_,_];
	Ps=[h(ashlyn,_,_),_,_,_,h(_,palauan,_)];
	Ps=[h(ashlyn,_,_),_,_,h(_,palauan,_),_];
	Ps=[h(ashlyn,_,_),_,h(_,palauan,_),_,_];
	Ps=[h(ashlyn,_,_),h(_,palauan,_),_,_,_]),

                                                   %$11
	((Ps=[_,_,_,h(tony,_,_),_],
	member(h(mckenna,_,vaughan),Ps));
	(Ps=[_,_,_,h(mckenna,_,_),_],
	member(h(tony,_,vaughan),Ps))).