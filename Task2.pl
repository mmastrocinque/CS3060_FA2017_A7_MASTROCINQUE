%% 1. Bryce went on vacation after Sawyer.
%% 2. The 5 people were  the person who started the rickrolling craze, the vacationer who left on May 31,
%% the person who received the dining table, Walter,  and the person who started the lolcat craze.
%% 3. The person who received the microwave is Bryce.
%% 4. Jane went on vacation after the person who received the juice press.
%% 5. Of Walter and the person who started the dancing baby craze, one left for vacation on August 29 and the other loved the microwave they received.
%% 6. The person who started the exploding whale craze is Walter.
%% 7. The person who received the juice press went on vacation before the person who received the coffee maker.
%% 8. Either the vacationer who left on February 20 or the vacationer who left on August 29 started the will it blend craze.
%% 9. The person who received the juice press didn't start the lolcat craze.
%% 10. The person who received the coffee maker is not Jane.
%% 11. The person who started the rickrolling craze is not Sawyer.

people(Ps) :-
% each person in the list Ps of people is represented as:
	%      h(FirstName,internet craze,wedding gift)

	length(Ps, 5),

	%% Define attributes

	member(h(bryce,_,_),Ps),
	member(h(donald,_,_),Ps),
	member(h(jane,_,_),Ps),
	member(h(sawyer,_,_),Ps),
	member(h(walter,_,_),Ps),
	member(h(_,dancing_baby,_),Ps),
	member(h(_,exploding_whale,_),Ps),
	member(h(_,lolcat,_),Ps),
	member(h(_,rickrolling,_),Ps),
	member(h(_,will_it_blend,_),Ps),
	member(h(_,_,blender),Ps),
	member(h(_,_,coffee_maker),Ps),
	member(h(_,_,dining_table),Ps),
	member(h(_,_,juice_press),Ps),
	member(h(_,_,microwave),Ps),

	(Ps=[_,_,_,h(sawyer,_,_),h(bryce,_,_)];		   %%1
	Ps=[_,_,h(sawyer,_,_),_,h(bryce,_,_)];
	Ps=[_,_,h(sawyer,_,_),h(bryce,_,_),_];
	Ps=[_,h(sawyer,_,_),_,_,h(bryce,_,_)];
	Ps=[_,h(sawyer,_,_),_,h(bryce,_,_),_];
	Ps=[_,h(sawyer,_,_),h(bryce,_,_),_,_];
	Ps=[h(sawyer,_,_),_,_,_,h(bryce,_,_)];
	Ps=[h(sawyer,_,_),_,_,h(bryce,_,_),_];
	Ps=[h(sawyer,_,_),_,h(bryce,_,_),_,_];
	Ps=[h(sawyer,_,_),h(bryce,_,_),_,_,_]),

	\+member(h(walter,rickrolling,_),Ps),           %%2
	\+member(h(walter,lolcat,_),Ps),
	\+member(h(walter,_,dining_table),Ps),
	\+member(h(_,lolcat,dining_table),Ps),
	\+member(h(_,rickrolling,dining_table),Ps),
	\+Ps=[_,_,h(walter,_,_),_,_],
	\+Ps=[_,_,h(_,rickrolling,_),_,_],
	\+Ps=[_,_,h(_,lolcat,_),_,_],
	\+Ps=[_,_,h(_,_,dining_table),_,_],

	member(h(bryce,_,microwave),Ps),                   %%3


	(Ps=[_,_,_,h(_,_,juice_press),h(jane,_,_)];		%%4
	Ps=[_,_,h(_,_,juice_press),_,h(jane,_,_)];
	Ps=[_,_,h(_,_,juice_press),h(jane,_,_),_];
	Ps=[_,h(_,_,juice_press),_,_,h(jane,_,_)];
	Ps=[_,h(_,_,juice_press),_,h(jane,_,_),_];
	Ps=[_,h(_,_,juice_press),h(jane,_,_),_,_];
	Ps=[h(_,_,juice_press),_,_,_,h(jane,_,_)];
	Ps=[h(_,_,juice_press),_,_,h(jane,_,_),_];
	Ps=[h(_,_,juice_press),_,h(jane,_,_),_,_];
	Ps=[h(_,_,juice_press),h(jane,_,_),_,_,_]),

                                                    %%5
	((Ps=[_,_,_,_,h(walter,_,_)],
	member(h(_,dancing_baby,microwave),Ps));
	(Ps=[_,_,_,h(_,dancing_baby,_),_],
	member(h(walter,_,microwave),Ps))),

                                                   %%6
	member(h(walter,exploding_whale,_),Ps),

	(Ps=[_,_,_,h(_,_,juice_press),h(_,_,coffee_maker)];		%%7
	Ps=[_,_,h(_,_,juice_press),_,h(jane,_,_)];
	Ps=[_,_,h(_,_,juice_press),h(_,_,coffee_maker),_];
	Ps=[_,h(_,_,juice_press),_,_,h(_,_,coffee_maker)];
	Ps=[_,h(_,_,juice_press),_,h(_,_,coffee_maker),_];
	Ps=[_,h(_,_,juice_press),h(_,_,coffee_maker),_,_];
	Ps=[h(_,_,juice_press),_,_,_,h(_,_,coffee_maker)];
	Ps=[h(_,_,juice_press),_,_,h(_,_,coffee_maker),_];
	Ps=[h(_,_,juice_press),_,h(_,_,coffee_maker),_,_];
	Ps=[h(_,_,juice_press),h(_,_,coffee_maker),_,_,_]),

	((Ps=[_,_,_,_,h(_,will_it_blend,_)]);
	  Ps=[h(_,will_it_blend,_),_,_,_,_]),

	\+member(h(_,lolcat,juice_press),Ps),
	\+member(h(jane,_,coffee_maker),Ps),
	\+member(h(sawyer,rickrolling,_),Ps).